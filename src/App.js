import { Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import AppNav from './components/AppNav';
import ListOfProducts from './components/ListOfProducts';
import getProducts from './services/getProducts';

function App() {

  const [product, setProduct] = useState([]);

  useEffect(function ()  {
    getProducts('2')
    .then(respo => setProduct(respo))
    .catch(res => console.log(res));
  },[])

  return (
    <div className="App">
        <AppNav setProduct={setProduct}/>
        {product.length === 0 ? <Typography variant='h2' align='center'>Producto no encontrado !</Typography> : <ListOfProducts product={product}/>}
    </div>
  )
}



export default App;
