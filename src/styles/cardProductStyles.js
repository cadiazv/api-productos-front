//import { withStyles } from '@material-ui/core/styles';

const styes = {
    item:{
        minWidth: "300px",
        margin: "1.5em",
        boxSizing: "border-box",
        textAlign: "center"
    },
    brand:{
        align: 'left',
        marginLeft: "10%",
        textAlign: 'left'
    },
    price:{
        textAlign: 'left',
        marginLeft: '10%',
    },
    originalPrice:{
        marginLeft: '1.5em',
        fontSize: 'small'
    },
    cartButton:{
        color: "green"
    }
}

export default styes;