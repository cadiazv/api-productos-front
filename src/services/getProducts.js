const apiURL = 'http://localhost:5000/products';

const getProducts  = async (product) => {

  const res = await fetch(apiURL, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ 'producto': product })
  })

  if(res.status === 500){
    return []
  }else{
    return res.json();
  }  
}

export default getProducts;