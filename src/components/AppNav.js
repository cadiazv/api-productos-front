import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Buttom from '@material-ui/core/Button';
import logo from '../Lider_logo.svg';
import useStyles from '../styles/appNavStyles';
import getProducts from '../services/getProducts';


export default function PrimarySearchAppBar(props) {
  const classes = useStyles();
  const [searchProduct, setSearchProduct] = React.useState("");

  const handleSubmit = e => {
    e.preventDefault();
    getProducts(e.target[0].value).then(response => props.setProduct(response));
  };

  const handleChange = e => {
    setSearchProduct(e.target.value);
  };

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
        <Typography className={classes.title} variant="h6" noWrap>
            <Buttom color="inherit">
                <img src={logo} alt="logo" className={classes.logo}/>
            </Buttom>
          </Typography>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon />
            <div className={classes.category}>
              Categorias
            </div>
          </IconButton>
          <div className={classes.search}>
          
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <form className={classes.searchInput} onSubmit={handleSubmit}>
            <InputBase
              onChange={handleChange}
              value={searchProduct}
              placeholder="Buscar Producto..."
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
            </form>
          </div>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-haspopup="true"
              color="inherit"
            >
            <AccountCircle />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}