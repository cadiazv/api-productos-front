import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Card, Typography, CardActions, Button, CardHeader } from '@material-ui/core';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import styles from '../styles/cardProductStyles';

function CardProduct ({image, id, price, brand, descuento, description, originalPrice, classes}) {

    return (
        <Card className={classes.item}>
            <img src={'https://'+image} height="80" alt={id}/>
            <Typography>
                <CardHeader className={classes.brand}
                    title={brand} 
                    subheader={description}               
                />
            </Typography>
            <Typography className={classes.price}>
                <div>
                {'$ '+price+'  '}
                {descuento === 0 ? '' : <strike className={classes.originalPrice}>{'$ '+originalPrice}</strike> }
                </div>
                {descuento === 0 ? '' : 'Descuento : '+descuento+' %'}
            </Typography>
            <Typography>
                
            </Typography>
            <CardActions>
                <Button size="small" color="primary">
                    Despacho
                </Button>
                <Button size="small" color="primary">
                    Retiro
                </Button>
                <Button className={classes.cartButton} size="small" >
                    Agregar
                    <ShoppingCartIcon/>
                </Button>
            </CardActions>
        </Card>
            
    )
    
}

export default withStyles(styles)(CardProduct)
