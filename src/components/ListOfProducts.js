import { Grid } from '@material-ui/core';
import React from 'react';
import CardProduct from './CardProduct';

export default function ListOfProducts ({product}) {

    return (
        
        <Grid container spacing={24} justify="center">
            {product.map(item => 
              <CardProduct
                key={item.id}
                image={item.image} 
                id={item.id}
                price={item.price}
                brand={item.brand}
                descuento={item.descuento}
                description={item.description}
                originalPrice={item.originalPrice}
              />       
            )}
        </Grid>
        )
}