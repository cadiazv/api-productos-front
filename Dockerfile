FROM node:15.3.0 

RUN mkdir -p /usr/src/front-app

COPY . /usr/src/front-app

WORKDIR /usr/src/front-app

RUN npm install

CMD ["npm", "start"]